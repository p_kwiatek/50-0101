<h2 class="main-header"><?php echo $pageName; ?></h2>
<div class="main-text">
    <?php
    foreach ($menuType as $k) {
        if ($k['active'] == 1) {
            ?>
            <h3 class="main-subheader"><?php echo $k['name'] ?></h3>
            <?php
            get_menu_tree($k['menutype'], 0, 0, 'sitemap');
        }
    }
    ?>
</div>