<h2><?php echo $pageName?></h2>
<div class="main-text">
<?php
echo $message;

if ($showLoginForm)
{
    include( CMS_TEMPL . DS . 'form_login.php');
}
		
if ($showArticle)
{
    echo '<div class="leadArticle">' . $article['lead_text'] . '</div>';
    
    echo $article['text'];
		
    if (! check_html_text($article['author'], '') )
    {
	?>
	<p class="author-name"><?php echo __('author')?>: <?php echo $article['author']?></p>
	<?php
    }
    
    ?>
</div>
<div>
    <?php
    /*
     *  Wypisanie plikow do pobrania
     */
    if ($numFiles > 0)
    {
	?>
	<div class="files-wrapper row">
            <div class="col-xs-12">
                <h3 class="files-header"><?php echo __('files')?></h3>
                <ul class="list-unstyled">
                <?php
                foreach ($outRowFiles as $row)
                {
                    $target = 'target="_blank" ';

                    if (filesize('download/'.$row['file']) > 5000000)
                    {
                        $url = 'download/'.$row['file'];
                    } else
                    {
                        $url = 'index.php?c=getfile&amp;id='.$row['id_file'];
                    }
                    if (trim($row['name']) == '')
                    {
                        $name = $row['file'];
                    } else
                    {
                        $name = $row['name'];
                    }			
                    $size = file_size('download/'.$row['file']);	
                    ?>
                    <li>
                        <a href="<?php echo $url?>" <?php echo $target?>>
                            <i class="icon-doc-text-inv icon" aria-hidden="true"></i>
                            <span class="title">
                                <?php echo $name?>
                                <span class="size">(<?php echo $size?>)</span>
                            </span>
                        </a>
                    </li>
                    <?php
                }
                ?>
                </ul>
            </div>
	</div>
    <?php
    }
		
    /*
     *  Wypisanie zdjec
     */
    if ($numPhotos > 0)
    {	
	$i = 0;
	?>
	<div class="gallery-wrapper row">
            <div class="col-xs-12">
                <h3 class="gallery-header"><?php echo __('gallery')?></h3>
                <ul class="list-unstyled row gallery">
                <?php
                foreach ($outRowPhotos as $row)
                {
                    $i++;
                    $noMargin = '';
                    if ($i == $pageConfig['zawijaj'])
                    {
                        $noMargin = ' noMargin';
                    }
                    ?>
                    <li class="col-xs-6 col-md-4 <?php echo $noMargin?>">
                        <a href="files/<?php echo $lang?>/<?php echo $row['file']?>" data-fancybox-group="gallery" class="photo fancybox">
                            <span class="gallery__imgholder">
                                <img src="files/<?php echo $lang?>/mini/<?php echo $row['file']?>" alt="<?php echo __('enlarge image') . ': ' . $row['name']?>" />
                            </span>
                            <div class="shape">
                                <div class="shape-img">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="277" height="209" viewBox="0 0 277 209">
                                        <pattern id="<?php echo 'gallery-image-' . ($i + 1); ?>" patternUnits="objectBoundingBox" width="100%" height="100%">
                                            <image xlink:href="files/<?php echo $lang?>/mini/<?php echo $row['file']?>" x="0" y="0" width="100%" height="100%" preserveAspectRatio="none" />
                                        </pattern>
                                        <path fill="<?php echo 'url(#gallery-image-' . ($i + 1) . ')' ?>" d="M524,1124l277,8.26L786,1333l-246-7Z" transform="translate(-524 -1124)" />
                                    </svg>
                                </div>
                            </div>
                        </a>
                        <?php
                        if (! check_html_text($row['name'], '') )
                        {
                            ?>
                            <p class="photo-name" aria-hidden="true">
                                <svg viewBox="0 0 186 40" preserveAspectRatio="none"><use xlink:href="#base-button"></use></svg>
                                <span><?php echo $row['name']?></span>
                            </p>
                            <?php
                        }
                        ?>
                    </li>
                <?php
                }
                ?>
                </ul>
            </div>
	</div>
    <?php
    }		
    if ($outSettings['pluginTweet'] == 'włącz')
    {
	 echo '<div class="Tweet"><iframe frameborder="0" scrolling="no" src="//platform.twitter.com/widgets/tweet_button.html" style="width:80px; height:30px;"></iframe></div>';  
    }

    if ($outSettings['pluginFB'] == 'włącz')
    {
        $color = 'light';
        if ($_SESSION['contr'] == 1)
        {
            $color = 'dark';
        }        
	$fb_url = urlencode('http://'.$pageInfo['host'].'/index.php?c=article&amp&id='. $_GET['id']);
	echo '<div class="FBLike"><iframe src=\'http://www.facebook.com/plugins/like.php?href='.$fb_url.'&amp;layout=standard&amp;show_faces=true&amp;width=400&amp;action=like&amp;font=tahoma&amp;colorscheme='.$color.'&amp;height=32&amp;show_faces=false\' scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:400px; height:32px;"></iframe></div>';   
    }
}
?>
</div>