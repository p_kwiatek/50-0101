<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
<!--[if lte IE 8]>
<script type="text/javascript">
    window.location = "<?php echo $pathTemplate?>/ie8.php";
</script>
<![endif]-->
<head>
<meta charset="UTF-8" />
<title><?php echo $pageTitle; ?></title>
<link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700%7CTitillium+Web&amp;subset=latin-ext" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes">
<meta name="description" content="<?php echo $pageDescription; ?>" />
<meta name="keywords" content="<?php echo $pageKeywords; ?>" />
<meta name="author" content="<?php echo $cmsConfig['cms']; ?>" />
<meta name="revisit-after" content="3 days" />
<meta name="robots" content="all" />
<meta name="robots" content="index, follow" />

<?php

array_unshift($css, 'vendor/bootstrap/css/bootstrap.min.css');

$jquery = array_shift($js);
array_unshift($js, 'owl.carousel.min.js');
array_unshift($js, 'ismobile.js');
array_unshift($js, 'modernizr.js');
array_unshift($js, 'vendor/snap/snap.svg.min.js');
array_unshift($js, 'vendor/bootstrap/js/bootstrap.min.js');
array_unshift($js, $jquery);

$pathTemplate = 'http://' . $pageInfo['host'] . '/' . $templateDir;
	
foreach ($js as $k => $v) {
    if (strpos($v, 'vendor') !== FALSE) {
        $path = $pathTemplate .'/' . $v;
    } else {
        $path = $pathTemplate .'/js/' . $v;
    }
    
    echo '<script type="text/javascript" src="'. $path . '"></script>' . "\r\n";
    
}
?>

<?php
foreach ($css as $k => $v) {
    if (strpos($v, 'vendor') !== FALSE) {
        echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/' . $v . '"/>' . "\r\n";
    } else {
        echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/css/' . $v . '"/>' . "\r\n";
    }
    
    if ($v == 'style.css')
    {
	$popupBackground = '#fff';
        $mainColor = $templateConfig['mainColor'];
        $overlayColor = $templateConfig['overColor'];
        $highColor = $templateConfig['highColor'];
			
	// wersja zalobna
	if ($outSettings['funeral'] == 'włącz') 
	{
	    $popupBackground = $templateConfig['popupBackground-bw'];
	    $mainColor = $templateConfig['mainColor-bw'];
	    $overlayColor = $templateConfig['overColor-bw'];
	    $highColor = $templateConfig['highColor-bw'];
	    echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/bw/css/style.css"/>' . "\r\n";
	}		
    }
		
    if ($v == 'jquery.fancybox.css')
    {
        if ($outSettings['funeral'] == 'włącz') 
        {
	    echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/bw/css/jquery.fancybox.css"/>' . "\r\n";
	}
    }
    if ($v == 'addition.css')
    {
	if ($outSettings['funeral'] == 'włącz')
	{
	    echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/bw/css/addition.css"/>' . "\r\n";
	}
    }      
}

$contrast = '';
if ($_SESSION['contr'] == 1)
{
    $contrast = 'flashvars.contrast = 1;' . "\r\n";
    $popupBackground = $templateConfig['popupBackground-ct'];
    $mainColor = $templateConfig['mainColor-ct'];
    $overlayColor = $templateConfig['overColor-ct'];
    $highColor = $templateConfig['highColor-ct'];	    
    echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/contrast/css/style.css"/>' . "\r\n";
    echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/contrast/css/jquery.fancybox.css"/>' . "\r\n";
    echo '<link rel="stylesheet" media="all" type="text/css" href="'. $pathTemplate .'/contrast/css/addition.css"/>' . "\r\n";
}	
	
echo '<link rel="shortcut icon" href="http://' . $pageInfo['host'] . '/' . $templateDir . '/images/favicon.ico" />' . "\r\n";

switch ($_SESSION['style']) {
    case '1' :
        ?>
        <link rel="stylesheet" media="all" type="text/css" href="<?php echo $pathTemplate?>/css/font-1.css"/>
        <?php
        break;
    case '2' :
        ?>
        <link rel="stylesheet" media="all" type="text/css" href="<?php echo $pathTemplate?>/css/font-2.css"/>
        <?php        
        break;
}
?>
<script type="text/javascript">
    texts = {
        image: '<?php echo __('image')?>',
        enlargeImage: '<?php echo __('enlarge image')?>',
        closeGallery: '<?php echo __('close gallery')?>',
        prevGallery: '<?php echo __('prev gallery')?>',
        nextGallery: '<?php echo __('next gallery')?>'
    };
    
    settings = {
        overlayColor: '<?php echo $overlayColor; ?>',
        transition: '<?php echo $outSettings['animType']?>',
        animationDuration: <?php echo $outSettings['transition']?>,
        duration: <?php echo $outSettings['duration']?>,
        showClock: false
    };
</script>
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-3924208-1']);
    _gaq.push(['_setDomainName', '.szkolnastrona.pl']);
    _gaq.push(['_setAllowHash', false]);
    _gaq.push(['_trackPageview']);

    (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
</head>
<body>
<svg xmlns="http://www.w3.org/2000/svg" width="186" height="40" viewBox="0 0 186 40" preserveAspectRatio="none" class="base">
    <path d="M259,2390l186-3-9-36-169-1Z" transform="translate(-259 -2350)" id="base-button" />
</svg>
