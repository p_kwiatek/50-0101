<?php
if ($showAll)
{
    ?>
<div class="gallery-wrapper row">
    <div class="col-xs-12">
        <h2><?php echo $pageName?></h2>
        <ul class="list-unstyled row gallery">
        <?php
        if (count($albums) > 0)
        {
            $n = 0;
            foreach ($albums as $value)
            {
                $n++;
                ?>
                <li class="col-xs-12 col-sm-6 col-md-4 <?php echo $noMargin?>">
                    <a href="<?php echo $value['link']?>" class="photo">
                        <span class="gallery__imgholder">
                            <img src="files/<?php echo $lang?>/mini/<?php echo $value['file']?>" alt="<?php echo __('enlarge image') . ': ' . $value['name']?>" />
                        </span>
                        <div class="shape">
                            <div class="shape-img">
                                <svg xmlns="http://www.w3.org/2000/svg" width="277" height="209" viewBox="0 0 277 209">
                                    <pattern id="<?php echo 'gallery-image-' . ($n + 1); ?>" patternUnits="objectBoundingBox" width="100%" height="100%">
                                        <image xlink:href="files/<?php echo $lang?>/mini/<?php echo $value['file']?>" x="0" y="0" width="100%" height="100%" preserveAspectRatio="none" />
                                    </pattern>
                                    <path fill="<?php echo 'url(#gallery-image-' . ($n + 1) . ')' ?>" d="M524,1124l277,8.26L786,1333l-246-7Z" transform="translate(-524 -1124)" />
                                </svg>
                            </div>
                        </div>
                    </a>
                    <p class="photo-name" aria-hidden="true">
                        <svg viewBox="0 0 186 40" preserveAspectRatio="none"><use xlink:href="#base-button"></use></svg>
                        <span><?php echo $value['name']?></span>
                    </p>
                </li>
                <?php
            }	
        } else
        {
            ?>
            <p><?php echo __('no photo album added')?></p>
            <?php
        }
        ?>
    </div>
</div>
    <?php
}
if ($showOne)
{
    ?>
<div class="gallery-wrapper row">
    <div class="col-xs-12">
        <h2><?php echo $pageName?></h2>
        <?php 
        echo $message;
        ?>
	<?php 
	if ($showGallery)
	{
	    if (count($outRows) > 0)
	    {
                ?>
                <ul class="list-unstyled row gallery">
                <?php
		$n = 0;
		foreach ($outRows as $value)
		{
		    $n++;
		    ?>
		    <li class="col-xs-12 col-sm-6 col-md-4 <?php echo $noMargin?>">
            <a href="files/<?php echo $lang?>/<?php echo $value['file']?>" data-fancybox-group="gallery" class="photo fancybox">
                <span class="gallery__imgholder">
                    <img src="files/<?php echo $lang?>/mini/<?php echo $value['file']?>" alt="<?php echo __('enlarge image') . ': ' . $value['name']?>" />
                </span>
                <div class="shape">
                    <div class="shape-img">
                        <svg xmlns="http://www.w3.org/2000/svg" width="277" height="209" viewBox="0 0 277 209">
                            <pattern id="<?php echo 'gallery-image-' . ($n + 1); ?>" patternUnits="objectBoundingBox" width="100%" height="100%">
                                <image xlink:href="files/<?php echo $lang?>/mini/<?php echo $value['file']?>" x="0" y="0" width="100%" height="100%" preserveAspectRatio="none" />
                            </pattern>
                            <path fill="<?php echo 'url(#gallery-image-' . ($n + 1) . ')' ?>" d="M524,1124l277,8.26L786,1333l-246-7Z" transform="translate(-524 -1124)" />
                        </svg>
                    </div>
                </div>
            </a>
			<?php
			if (! check_html_text($value['name'], '') )
			{
			    ?>
			    <p class="photo-name">
                    <svg viewBox="0 0 186 40" preserveAspectRatio="none"><use xlink:href="#base-button"></use></svg>
                    <span><?php echo $value['name']?></span>
                </p>
			    <?php
			}
		    ?>
		    </li>
                <?php
		}
                ?>
                </ul>
                <?php
	    }
        }
    ?>
    </div>
</div>
<?php
if ($showLoginForm)
{
    ?>
    <div class="main-text">
    <?php
    include( CMS_TEMPL . DS . 'form_login.php');
    ?>
    </div>
    <?php
}
?>
    
<?php
}
?>
