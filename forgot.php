<?php
if ($showForgotForm){
?>
<a id="wyslij"></a>
<h2><?php echo $pageName; ?></h2>
<form name="forgotForm" id="forgotForm" class="" method="post" action="<?php echo $url; ?>,nowe-haslo#wyslij">
    <fieldset>
	<?php
	echo $message;
	?>
	<legend><?php echo __('new password'); ?></legend>
	
	<p><?php echo __('generate new password info'); ?></p>
	
	<div class="formL"><label for="email" class="formLabel"><?php echo __('email'); ?>:</label></div>
	<div class="formR"><input type="text" id="email" name="email" class="inText" size="35" maxlength="50" value="<?php echo $email; ?>" /><span id="emailError" class="msgMarg"></span></div>
	<br class="clear" />
	
	<div class="formL"></div>
	<div class="formR"><input type="submit" name="ok" value="<?php echo __('send'); ?>" class="butForm"/></div>
	<br class="clear" />	
	
    </fieldset>
</form>

<script type="text/javascript">
    $(document).ready(function() {
	var form = $('#forgotForm');
	form.submit(function(){
	    if (validateEmail()){
		//return true;
	    } else {
		return false;
	    }
	});
	
	$('#email').blur(validateEmail);
	function validateEmail(){
	    var exp = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
	    var email = $("#email").val();
	    if (!exp.test(email)){
		$('#email').addClass('inError');
		$('#emailError').addClass('msgError').text('<?php echo __('error incorrect email'); ?>');
		return false;
	    } else {
		$('#email').removeClass('inError');
		$('#emailError').removeClass('msgError').text('');
		return true;
	    }
	}	
    });
</script>

<?php
}
if ($showForgotInfo){
?>

<h2><?php echo $pageName; ?></h2>
<div class="main-text">
    <?php echo $message; ?>
</div>

<div class="row">
    <ul class="list-unstyled list-inline col-xs-12 back-links">
        <li><a href="forum" class="button"><?php echo __('forum home page') ?></a></li>
        <li><a href="index.php" class="button"><?php echo __('home page') ?></a></li>
    </ul>
    <div class="clearfix"></div>
</div>

<?php
}
?>

