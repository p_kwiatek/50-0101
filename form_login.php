<form class="form-login" action="index.php?c=<?php echo $_GET['c'].'&amp;id='.$_GET['id']; ?>" method="post">
	<?php 

		// Potrzebne do sprawdzenia podczas logowania czy user ma uprawnienia do stron chronionych i forum
	
		switch ($_GET['c']) {
			case 'page' : 
			case 'article' : 
					$pValue = "text"; 
					break;
			case 'modules' : 
					$pValue = "forum"; 
					break;
		}
	?>
    <input type="hidden" name="protect" value="<?php echo $pValue; ?>" />
    
	<fieldset>
    	<legend><?php echo __('login action'); ?></legend>
      
	<div class="formL">
	    <label for="fLogin" class="formLabel"><?php echo __('login'); ?>:</label>
	</div>
	<div class="formR">
	    <input type="text" class="inText" name="fLogin" id="fLogin" value="" /><span id="loginMsg" class="msgMarg"></span>
	</div>
	<br class="clear" />
	
	<div class="formL">
	    <label for="fPass" class="formLabel"><?php echo __('password'); ?>:</label>
	</div>
	<div class="formR">
	    <input type="password" class="inText" name="fPass" id="fPass" value="" /><span id="passwordMsg" class="msgMarg"></span>
	</div>
	<br class="clear" />     
        
	<div class="formL"></div>
	<div class="formR">
	    <input type="submit" name="loginUser" value="<?php echo __('login action'); ?>" class="butForm"/>
            <div class="remind-passowrd-wrapper"><a href="generuj-haslo"><?php echo __('password forgot'); ?></a></div>
	</div>
	<br class="clear" />
	
    </fieldset>
</form>

<script type="text/javascript">
    $(document).ready(function() {
	$('#fLogin').blur(validateLogin);
	function validateLogin(){
	    if ($('#fLogin').val().length < 4){
		$('#fLogin').addClass('inError');
		$('#loginMsg').addClass('msgError').text('<?php echo __('error min length login'); ?>');
		return false;
	    } else {
		$('#fLogin').removeClass('inError');
		$('#loginMsg').removeClass('msgError').text('');
		return true;
	    }
	}
	
	$('#fPass').blur(validatePassword);
	function validatePassword(){
	    if ($('#fPass').val().length < 8){
		$('#fPass').addClass('inError');
		$('#passwordMsg').addClass('msgError').text('<?php echo __('error min length password'); ?>');
		return false;
	    } else {
		$('#fPass').removeClass('inError');
		$('#passwordMsg').removeClass('msgError').text('');		
		return true;
	    }
	}
	
    });
</script>