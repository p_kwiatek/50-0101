<div class="class-list">
<?php if ($showClass): ?>
    <h2><?php echo $pageName . ' ' . __('for') . ' ' . $outRowTT['class']; ?></h2>
    <div class="main-text"><?php echo $outRowTT['text']?></div>
    <div class="main-text">
        <?php foreach($arrSchoolWeek as $k => $v): ?>
        <div class="table-responsive">
            <table class="timetable">
                <caption class="text-left"><?php echo $v['long']?></caption>
                <tr>
                    <th class="timetable-no" style="width: 5%;"><?php echo __('no')?></th>
                    <th class="timetable-hour" style="width: 15%;"><?php echo __('hour')?></th>
                    <th class="timetable-lesson" style="width: 35%;"><?php echo __('lesson')?></th>
                    <th class="timetable-teacher" style="width: 35%;"><?php echo __('teacher')?></th>
                    <th class="timetable-room" style="width: 10%;"><?php echo __('room')?></th>
                </tr>
                <?php
                $countHour = 0;
                for ($i = 0; $i < 10; $i++) {
                    if (trim($dayPlan[$v['short']][$i]) != '') {
                        $countHour++;
                        ?>
                        <tr>
                            <td class="timetable-no"><?php echo ($i+1)?>.</td>
                            <td class="timetable-hour"><?php echo $hours[$v['short']][$i]?></td>
                            <td class="timetable-lesson"><?php echo $dayPlan[$v['short']][$i]?></td>
                            <td class="timetable-teacher"><?php echo $teacher[$v['short']][$i]?></td>
                            <td class="timetable-room"><?php echo $room[$v['short']][$i]?></td>
                        </tr>
                        <?php
                    }
                }
                
                if ($countHour <= 0) {
                    ?>
                    <tr><td colspan="5"><p class="txt_err txt-err"><?php echo __('no lessons')?></p></td></tr>
                    <?php
                }
                ?>
            </table>
        </div>
        <?php endforeach; ?>
    </div>
<?php
    elseif ($numRows > 0):
?>
    <h2><?php echo $pageName?></h2>
    <ul>
    <?php foreach ($outRowTT as $row): ?>
	<li>
	    <a href="plan_lekcji_<?php echo $row['id']?>">
                <i class="icon-calendar icon" aria-hidden="true"></i>
                <span class="title"><?php echo $row['class']?></span>
            </a>
	</li>
    <?php endforeach; ?>
    </ul>
<?php
    endif;
?>

</div>