<div class="main-text">
<h2><?php echo $pageName; ?></h2>
<?php
// Wypisanie artykulow
if ($numArticles > 0)
{	
    $i = 0;
    ?>
    <div class="article-wrapper">
    <h2 class="sr-only"><?php echo __('articles')?></h2>
    <?php
    foreach ($outArticles as $row)
    {
	$highlight = $url = $target = $url_title = $protect = '';
			
	if ($row['protected'] == 1)
	{
	    $protect = '<i class="icon-protected icon"></i>';
	    $url_title = ' title="' . __('page requires login') . '"';
	}				
			
	if (trim($row['ext_url']) != '')
	{
	    if ($row['new_window'] == '1')
	    {
		$target = ' target="_blank"';
	    }	
	    $url_title = ' title="' . __('opens in new window') . '"';
	    $url = ref_replace($row['ext_url']);					
	} else
	{
	    if ($row['url_name'] != '')
	    {
		$url = 'a,' . $row['id_art'] . ',' . $row['url_name'];
	    } else
	    {
		$url = 'index.php?c=article&amp;id=' . $row['id_art'];
	    }
	}	
									
	$margin = ' no-photo';
	if (is_array($photoLead[$row['id_art']]))
	{
	    $margin = '';
	}			
			
	$row['show_date'] = substr($row['show_date'], 0, 10);
        
        $highlight = '';
        if ($row['highlight'] == 1)
        {
            $highlight = ' highlight-article';
        }        
	?>
    <div class="article<?php echo $highlight?><?php if (!is_array($photoLead[$row['id_art']])): ?> no-photo<?php endif; ?>" id="<?php echo 'article-' . ($i + 1); ?>">
        <?php
            if (is_array($photoLead[$row['id_art']]))
            {
                $photo = $photoLead[$row['id_art']];
        ?>
        <a href="files/<?php echo $lang?>/<?php echo $photo['file']?>" title="<?php echo __('enlarge image') . ': ' . $row['name']; ?>" class="photo fancybox" data-fancybox-group="gallery">
            <div class="shape">
                <div class="shape-img">
                    <svg xmlns="http://www.w3.org/2000/svg" width="277" height="209" viewBox="0 0 277 209">
                        <pattern id="<?php echo 'article-image-' . ($i + 1); ?>" patternUnits="objectBoundingBox" width="100%" height="100%">
                            <image xlink:href="files/<?php echo $lang?>/mini/<?php echo $photo['file']?>" x="0" y="0" width="100%" height="100%" preserveAspectRatio="none" />
                        </pattern>
                        <path fill="<?php echo 'url(#article-image-' . ($i + 1) . ')' ?>" d="M524,1124l277,8.26L786,1333l-246-7Z" transform="translate(-524 -1124)" />
                    </svg>
                </div>
            </div>
        </a>
        <?php 
            }
        ?>
        <div class="lead-text">
            <h4>
                <a href="<?php echo $url?>" <?php echo $url_title . $target?>><?php echo $row['name'] . $protect?></a>
            </h4>
            <div class="lead-main-text">
                <?php echo truncate_html($row['lead_text'], 300, '...')?>
            </div>
            <?php if ($row['show_date'] != '' && $row['show_date'] != '0000-00-00') { ?>
                <p class="article-date">
                    <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 10 10">
                        <path d="M833.879,1854.11h-0.555v1.11H831.66v-1.11h-3.33v1.11h-1.665v-1.11H826.11a1.113,1.113,0,0,0-1.11,1.11v6.66a1.113,1.113,0,0,0,1.11,1.11h7.769a1.113,1.113,0,0,0,1.11-1.11v-6.66A1.113,1.113,0,0,0,833.879,1854.11Zm0,7.77H826.11v-4.44h7.769v4.44Zm-5.827-8.88h-1.109v1.94h1.109V1853Zm5,0h-1.11v1.94h1.11V1853Z" transform="translate(-825 -1853)" fill="#979797" />
                    </svg>
                    <?php echo $row['show_date'] ?>
                </p>
            <?php } ?>
            <div class="article-meta">
                <a href="<?php echo $url ?>" <?php echo $url_title . $target ?> class="button" tabindex="-1" title="">
                    <svg viewBox="0 0 186 40" preserveAspectRatio="none"><use xlink:href="#base-button"></use></svg>
                    <span class="text"><?php echo __('more') ?></span>
                    <span class="sr-only"> <?php echo __('about')?>: <?php echo $row['name'] . $protect?></span>
                </a>
            </div>
        </div>
    </div>
    <?php
        $i++;
    }
    ?>
    </div>
<?php
}
?>
</div>
