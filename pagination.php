<?php
if ($pagination['end'] > 1)
{
    ?>
<div class="row">
    <div class="pagination-wrapper col-xs-12">
        <?php
        $active_page = $pagination['active'];

        if ( !isset($pagination['active']) )
        {
            $active_page = 1;
        }
        ?>
        <p><?php echo __('page')?>: <strong><?php echo $active_page?></strong>/<?php echo $pagination['end']?></p>
        <ul class="list-unstyled list-inline">
        <?php
        if ($pagination['start'] != $pagination['prev'])
        {
            ?>
            <li>
                <a href="<?php echo $url . $pagination['start']?>" rel="nofollow" class="btn-first">
                    <svg xmlns="http://www.w3.org/2000/svg" width="33" height="35" viewBox="0 0 33 35" class="pagination__background">
                        <path d="M652,2197l33,4-3,28-28,3Z" transform="translate(-652 -2197)" />
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16.062" height="19" viewBox="0 0 16.062 19" class="pagination__arrow">
                      <defs>
                        <filter id="filter-arrow-double-right" x="746.969" y="2240" width="16.062" height="19" filterUnits="userSpaceOnUse">
                          <feOffset result="offset" dy="1" in="SourceAlpha"/>
                          <feGaussianBlur result="blur"/>
                          <feFlood result="flood" flood-color="#325e12"/>
                          <feComposite result="composite" operator="in" in2="blur"/>
                          <feBlend result="blend" in="SourceGraphic"/>
                        </filter>
                      </defs>
                      <path d="M746.981,2257.73l8.859-8.86-8.859-8.86h2.176l8.861,8.86-8.861,8.86h-2.176Zm5,0,8.859-8.86-8.859-8.86h2.176l8.862,8.86-8.862,8.86h-2.176Z" transform="translate(-746.969 -2240)" fill="#fff" filter="url(#filter-arrow-double-right)" />
                    </svg>
                    <span class="sr-only"><?php echo __('first page')?></span>
                </a>
            </li>
            <li>
                <a href="<?php echo $url . $pagination['prev']?>" rel="nofollow" class="btn-prev">
                    <svg xmlns="http://www.w3.org/2000/svg" width="33" height="35" viewBox="0 0 33 35" class="pagination__background">
                        <path d="M652,2197l33,4-3,28-28,3Z" transform="translate(-652 -2197)" />
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="11.062" height="19" viewBox="0 0 11.062 19" class="pagination__arrow">
                    <defs>
                        <filter id="filter-arrow-single-right" x="710.969" y="2219" width="11.062" height="19" filterUnits="userSpaceOnUse">
                          <feOffset result="offset" dy="1" in="SourceAlpha"/>
                          <feGaussianBlur result="blur"/>
                          <feFlood result="flood" flood-color="#325e12"/>
                          <feComposite result="composite" operator="in" in2="blur"/>
                          <feBlend result="blend" in="SourceGraphic"/>
                        </filter>
                      </defs>
                      <path  d="M710.981,2236.73l8.859-8.86-8.859-8.86h2.176l8.861,8.86-8.861,8.86h-2.176Z" transform="translate(-710.969 -2219)" filter="url(#filter-arrow-single-right)" fill="#fff" />
                    </svg>
                    <span class="sr-only"><?php echo __('prev page')?></span>
                </a>
            </li>
            <?php
        }		
        foreach ($pagination as $k => $v)
        {
            if (is_numeric($k))
            {
                ?>
            <li>
                <a href="<?php echo $url . $v?>" rel="nofollow" class="btn-page">
                    <svg xmlns="http://www.w3.org/2000/svg" width="33" height="35" viewBox="0 0 33 35" class="pagination__background">
                        <path d="M652,2197l33,4-3,28-28,3Z" transform="translate(-652 -2197)" />
                    </svg>
                    <span class="sr-only"><?php echo __('page')?></span>
                    <span class="text"><?php echo $v?></span>
                </a>
            </li>
                <?php
            } else if ($k == 'active')
            {
                ?>
            <li>
                <span class="page-active">
                    <span class="sr-only"><?php echo __('page')?></span>
                    <span class="text"><?php echo $v?></span>
                </span>
            </li>
                <?php
            }			
        }		
        if ($pagination['active'] != $pagination['end'])
        {
            ?>
            <li>
                <a href="<?php echo $url . $pagination['next']?>" rel="nofollow" class="btn-next">
                    <svg xmlns="http://www.w3.org/2000/svg" width="33" height="35" viewBox="0 0 33 35" class="pagination__background">
                        <path d="M652,2197l33,4-3,28-28,3Z" transform="translate(-652 -2197)" />
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="11.062" height="19" viewBox="0 0 11.062 19" class="pagination__arrow">
                      <defs>
                        <filter id="filter-arrow-single-right" x="710.969" y="2219" width="11.062" height="19" filterUnits="userSpaceOnUse">
                          <feOffset result="offset" dy="1" in="SourceAlpha"/>
                          <feGaussianBlur result="blur"/>
                          <feFlood result="flood" flood-color="#325e12"/>
                          <feComposite result="composite" operator="in" in2="blur"/>
                          <feBlend result="blend" in="SourceGraphic"/>
                        </filter>
                      </defs>
                      <path  d="M710.981,2236.73l8.859-8.86-8.859-8.86h2.176l8.861,8.86-8.861,8.86h-2.176Z" transform="translate(-710.969 -2219)" filter="url(#filter-arrow-single-right)" fill="#fff" />
                    </svg>
                    <span class="sr-only"><?php echo __('next page')?></span>
                </a>
            </li>
            <li>
                <a href="<?php echo $url . $pagination['end']?>" rel="nofollow" class="btn-last">
                    <svg xmlns="http://www.w3.org/2000/svg" width="33" height="35" viewBox="0 0 33 35" class="pagination__background">
                        <path d="M652,2197l33,4-3,28-28,3Z" transform="translate(-652 -2197)" />
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16.062" height="19" viewBox="0 0 16.062 19" class="pagination__arrow">
                      <defs>
                        <filter id="filter-arrow-double-right" x="746.969" y="2240" width="16.062" height="19" filterUnits="userSpaceOnUse">
                          <feOffset result="offset" dy="1" in="SourceAlpha"/>
                          <feGaussianBlur result="blur"/>
                          <feFlood result="flood" flood-color="#325e12"/>
                          <feComposite result="composite" operator="in" in2="blur"/>
                          <feBlend result="blend" in="SourceGraphic"/>
                        </filter>
                      </defs>
                      <path d="M746.981,2257.73l8.859-8.86-8.859-8.86h2.176l8.861,8.86-8.861,8.86h-2.176Zm5,0,8.859-8.86-8.859-8.86h2.176l8.862,8.86-8.862,8.86h-2.176Z" transform="translate(-746.969 -2240)" fill="#fff" filter="url(#filter-arrow-double-right)" />
                    </svg>
                    <span class="sr-only"><?php echo __('last page')?></span>
                </a>
            </li>
            <?php
        }
        ?>	
        </ul>
        <div class="clearfix"></div>
    </div>
</div>
    <?php
}
?>