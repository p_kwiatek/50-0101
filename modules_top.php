<?php if (count($outRowTopModules) > 0): ?>
<div class="headmodules">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="headmodules__list">
                    <?php
                    $i = 0;
                    foreach ($outRowTopModules as $module):
                        $i++;
                        $mod = 'module' . $i;
                    ?>
                    <li id="<?php echo $module['mod_name']; ?>" class="module <?php echo $mod; ?>">
                        <div>
                            <div class="headmodules__icon"></div>
                            <div class="headmodules__title--holder">
                                <h2>
                                    <svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" width="262" height="55" viewBox="0 0 262 55"><path d="M530,624l247,6,9,42-262,7Z" transform="translate(-524 -624)" /></svg>
                                    <span><?php echo $module['name']; ?></span>
                                </h2>
                            </div>
                            <div class="headmodules__content"><?php echo get_module($module['mod_name'], 'top'); ?></div>
                        </div>
                    </li>
                    <?php      
                        endforeach;
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>