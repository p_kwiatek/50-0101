<div id="toolbar" class="toolbar">
    <div class="container">
        <div class="row">
            <div class="col-md-12 clearfix">
                <ul class="toolbar__links">
                    <li>
                        <a href="index.php" title="<?php echo __("home page"); ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" width="12.125" height="10.75" viewBox="0 0 12.125 10.75">
                                <path d="M254.75,23.422L254,23.084v4.538a0.32,0.32,0,0,1-.328.345h-2.685l0-2.552a0.4,0.4,0,0,0-.454-0.422H249.38a0.364,0.364,0,0,0-.366.422l-0.005,2.56-2.741,0a0.258,0.258,0,0,1-.26-0.277V23.083l-0.792.365c-0.847.716-1.235,0-1.235,0l5.977-6.233,6.14,6.207S255.73,24.129,254.75,23.422Z" transform="translate(-243.969 -17.219)" fill="#458418" />
                            </svg><?php echo __("home page"); ?>
                        </a>
                    </li>
                    <li>
                        <a href="mapa_strony" title="<?php echo __("sitemap"); ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" width="9.969" height="11" viewBox="0 0 9.969 11">
                                <path d="M385.835,22.972a0.416,0.416,0,0,0,.416-0.416,0.506,0.506,0,0,0-.416-0.51h-2.392V21.113h1.22a0.416,0.416,0,0,0,.417-0.416v-3.27a0.416,0.416,0,0,0-.417-0.416H381.39a0.416,0.416,0,0,0-.417.416V20.7a0.417,0.417,0,0,0,.417.416h1.22v0.934h-2.454a0.507,0.507,0,0,0-.417.51,0.417,0.417,0,0,0,.417.416H380v0.918h-1.564a0.416,0.416,0,0,0-.416.416v3.27a0.416,0.416,0,0,0,.416.416h3.212a0.416,0.416,0,0,0,.416-0.416v-3.27a0.416,0.416,0,0,0-.416-0.416h-0.659V22.972H385v0.918h-0.658a0.416,0.416,0,0,0-.416.416v3.27a0.416,0.416,0,0,0,.416.416h3.243a0.416,0.416,0,0,0,.416-0.416v-3.27a0.416,0.416,0,0,0-.416-0.416h-1.627V22.972h-0.127Z" transform="translate(-378.031 -17)" fill="#458418" />
                            </svg><?php echo __("sitemap"); ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo $pageInfo['bip'] ?>" title="<?php echo __("bip"); ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" width="47.938" height="20.5" viewBox="0 0 47.938 20.5">
                                <path d="M515.632,18.257L526.8,29.6V18.257h-11.17Zm41.78-.007a6.144,6.144,0,0,0-6.139,6.137V32.96a1.5,1.5,0,1,0,3,0V29.635a6.065,6.065,0,0,0,3.141.893,6.139,6.139,0,0,0,0-12.278m0,9.278a3.14,3.14,0,1,1,3.141-3.141,3.143,3.143,0,0,1-3.141,3.141m-20.1-9.271a6.078,6.078,0,0,0-3.142.893V15.456a1.5,1.5,0,0,0-3,0V24.4s0,0.006,0,.01a6.139,6.139,0,1,0,6.137-6.15m0,9.28a3.14,3.14,0,1,1,3.141-3.14,3.145,3.145,0,0,1-3.141,3.14m11.538,0.152c-0.2-.1-0.815-0.411-0.815-2.794V19.768a1.5,1.5,0,1,0-3,0v5.127c0,1.816.255,4.36,2.463,5.475a1.5,1.5,0,0,0,1.349-2.681m-2.313-10.428a1.65,1.65,0,1,0-1.649-1.647,1.648,1.648,0,0,0,1.649,1.647" transform="translate(-515.625 -13.969)" fill="#c00418" />
                            </svg>
                            <span class="sr-only"><?php echo __("bip"); ?></span>
                        </a>
                    </li>
                </ul>
                <div class="toolbar__holder">
                    <div class="toolbar__fonts">
                        <span><?php echo __('font size'); ?></span>
                        <ul>
                            <?php
                                if ($_SESSION['contr'] == 0)
                                {
                                    $set_contrast = 1;
                                    $contrast_txt = __('contrast version');
                                } else {
                                    $set_contrast = 0;
                                    $contrast_txt = __('graphic version');
                                }
                            ?>
                            <li>
                                <a href="ch_style.php?style=0">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="8.875" height="9.969" viewBox="0 0 8.875 9.969">
                                        <path d="M887.945,28h2.017l-3.411-9.953h-2.044L881.082,28H883.1l0.676-2.147h3.494Zm-2.44-7.622h0.041l1.23,3.91h-2.508Z" fill="#458418" transform="translate(-881.094 -18.031)"/>
                                    </svg>
                                    <span class="sr-only"><?php echo __('font default')?></span>
                                </a>
                            </li>
                            <li>
                                <a href="ch_style.php?style=r1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="13.906" height="15" viewBox="0 0 13.906 15">
                                        <path d="M908.93,29h2.592l-4.385-12.8h-2.628l-4.4,12.8H902.7l0.87-2.76h4.492Zm-3.138-9.8h0.053l1.582,5.027H904.2ZM914,17h-2v2h-1V17h-2V16h2V14h1v2h2v1Z" fill="#458418" transform="translate(-900.094 -14)"/>
                                    </svg>
                                    <span class="sr-only"><?php echo __('font bigger')?></span>
                                </a>
                            </li>
                            <li>
                                <a href="ch_style.php?style=r2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="21.875" height="16" viewBox="0 0 21.875 16">
                                        <path d="M932.914,30h3.169l-5.36-15.641h-3.212L922.129,30H925.3l1.063-3.373h5.49Zm-3.835-11.978h0.065l1.933,6.144h-3.942ZM938,17h-2v2h-1V17h-2V16h2V14h1v2h2v1Zm6,0h-2v2h-1V17h-2V16h2V14h1v2h2v1Z" fill="#458418" transform="translate(-922.125 -14)"/>
                                    </svg>
                                    <span class="sr-only"><?php echo __('font biggest')?></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <a href="ch_style.php?contr=<?php echo $set_contrast ?>" title="<?php echo $contrast_txt; ?>" class="toolbar__contrast">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                            <path d="M977.007,18.992a5,5,0,1,0,5,5A5,5,0,0,0,977.007,18.992Zm0,8.125v-6.25A3.125,3.125,0,0,1,977.007,27.118Zm0,3.125a1.25,1.25,0,0,1,1.25,1.25v1.25a1.25,1.25,0,1,1-2.5,0v-1.25A1.25,1.25,0,0,1,977.007,30.243Zm0-12.5a1.25,1.25,0,0,1-1.25-1.25v-1.25a1.25,1.25,0,1,1,2.5,0v1.25A1.25,1.25,0,0,1,977.007,17.742Zm8.751,5a1.25,1.25,0,0,1,0,2.5h-1.25a1.25,1.25,0,1,1,0-2.5h1.25Zm-15,1.25a1.25,1.25,0,0,1-1.25,1.25h-1.25a1.25,1.25,0,0,1,0-2.5h1.25A1.25,1.25,0,0,1,970.757,23.993Zm12.438,4.42,0.884,0.884a1.25,1.25,0,0,1-1.768,1.768l-0.884-.884A1.25,1.25,0,0,1,983.2,28.412ZM970.82,19.573l-0.884-.884a1.25,1.25,0,1,1,1.768-1.768l0.884,0.884A1.25,1.25,0,0,1,970.82,19.573Zm12.375,0a1.25,1.25,0,0,1-1.768-1.768l0.884-.884a1.25,1.25,0,0,1,1.768,1.768ZM970.82,28.412a1.25,1.25,0,1,1,1.767,1.768l-0.883.884a1.25,1.25,0,1,1-1.768-1.768Z" fill="#458418" transform="translate(-967 -14)"/>
                        </svg><span><?php echo __('font contrast'); ?></span>
                    </a>
                    <div class="toolbar__search">
                        <a id="search" tabindex="-1"></a>
                        <h2 class="sr-only"><?php echo __('search')?></h2>
                        <form id="searchForm" name="f_szukaj" method="get" action="index.php">
                            <input name="c" type="hidden" value="search" />
                            <label for="kword"><span class="sr-only"><?php echo __('search query')?></span></label>
                            <input type="text" id="kword" name="kword" value="<?php echo __('search query'); ?>" onfocus="if (this.value=='<?php echo __('search query'); ?>') {this.value=''};" onblur="if (this.value=='') {this.value='<?php echo __('search query'); ?>'};"/>
                            <button type="submit" name="search">
                                <span><?php echo __('search action'); ?></span>
                                <svg xmlns="http://www.w3.org/2000/svg" width="95" height="36"  preserveAspectRatio="none" viewBox="0 0 95 36"><path d="M1353,41.985l-95-2.018,6-31.989L1345,6Z" transform="translate(-1258 -6)" /></svg>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
