<?php 
// konfiguracja strony
$pageConfig['zawijaj']	    = 4;
$pageConfig['swfWidth']	    = 456;
$pageConfig['swfHeight']    = 285;
$pageConfig['duration']	    = 1;
$pageConfig['transition']   = 1;  

//konfiguracja template
$templateConfig = array(
    'name'	    => '50-0101',
    'column'	    => 2,
    'modulesTop'    => 4,
    'modulesTop2'   => 3,
    'modulesBottom' => 3,
    'modulesRight'  => false,
    'userInModule'  => true,    
    'mainColor'	    => '#5f5fff',
    'highColor'	    => '#ff6c00',
    'overColor'	    => '#ffffff',
    'mainColor-bw'  => '#555555',
    'highColor-bw'  => '#f7f7f7',
    'overColor-bw'  => '#f7f7f7',
    'mainColor-ct'  => '#ffff00',
    'highColor-ct'  => '#111111',
    'overColor-ct'  => '#000000',
    'clockWidth'    => 60,
    'clockHeight'   => 60,
    'showClock'     => true,
    'maxWidthLeftAdv'   => 190,
    'maxWidthTopAdv'    => 700,
    'popupBackground-bw'=> '#ffffff',
    'popupBackground-ct'=> '#000000',
    'menuTopIcons' => false,
    'noFlash' => true,
    'bootstrap' => true,
);

//konfiguracja obrazków
$imageConfig = array(
    'maxWidth'	    => 900,
    'miniWidth'	    => 350,
    'miniHeight'    => 220,
    'proportional'  => 0,
    'jpgCompression'=> 95,
    'bgColor'	    => 'FFFFFF',
    'bannerWidth'   => 1140,
    'bannerHeight'  => 515,
    'avatarWidth'   => 50,
    'avatarHeight'  => 50,
    'avatarSize'    => 512000
);
	
$animType = array(
    'Przenikanie'   => 'fade',
    'Przenikanie z powiększeniem' => 'fadeUp',
    'Przenikanie z pomniejszeniem' => 'fadeDown',
    'Nasunięcie z góry' => 'goDown',
    'Nasunięcie z dołu' => 'goUp',
    'Przesunięcie w lewo' => 'backSlide'
);

?>
