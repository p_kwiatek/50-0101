<h2><?php echo $pageName; ?></h2>
<a id="zapisz" tabindex="-1" class="anchor"></a>
<h3 class="subHead"><?php echo __('newsletter sign in')?></h3>
<div class="main-text">
<?php
	if ($_GET['action'] == 'join' || $_GET['action'] == 'delmail') {
		echo $message;
	}
	else
	{
		if ($_GET['action'] == 'save' || $_GET['action'] == 'join' || $_GET['action'] == 'delmail') {
			echo $message;
		}
		?>
		
			<form name="f_newslet" id="addForm" class="formNewslet" method="post" action="newsletter_zapisz#zapisz">
			<fieldset>  
			    <legend><?php echo __('newsletter form sign in'); ?></legend>

			    <div class="txt_com"><span class="asterisk">*</span><?php echo __('required fields')?></div>

			    <div class="formL">
				<label for="addFirstName" class="formLabel"><span class="asterisk">*</span><?php echo __('firstname'); ?>: </label>
			    </div>
			    <div class="formR">
				<input id="addFirstName" type="text" class="inText" name="imie" size="35" maxlength="50" <?php echo $imie; ?> /><span id="addFirstNameMsg" class="msgMarg"></span>
			    </div>
			    <br class="clear" />

			    <div class="formL">
				<label for="addSecondName" class="formLabel"><span class="asterisk">*</span><?php echo __('lastname'); ?>: </label>
			    </div>
			    <div class="formR">
				<input id="addSecondName" type="text" class="inText"  name="nazwisko" size="35" maxlength="50" <?php echo $nazwisko; ?> /><span id="addSecondNameMsg" class="msgMarg"></span>
			    </div>
			    <br class="clear" />

			    <div class="formL">
				<label for="addEmail" class="formLabel"><span class="asterisk">*</span><?php echo __('email'); ?>: </label>
			    </div>
			    <div class="formR">
				<input id="addEmail" type="text" class="inText" name="email" size="35" maxlength="50" <?php echo $email; ?> /><span id="addEmailMsg" class="msgMarg"></span>
			    </div>
			    <br class="clear" />

			    <div class="formL"></div>
			    <div class="formR">
				<input type="submit" name="ok" value="<?php echo __('signin action'); ?>" class="butForm"/>
			    </div>
			    <br class="clear" />
				
			</fieldset>  
			</form>
			<br />    
		
			<a name="wypisz" class="anchor"></a>
		<h3 class="subHead"><?php echo __('newsletter sign out'); ?></h3>
		<?php
			if ($_GET['action'] == 'del') {
				echo '<div class="emptyMargin"></div> ' . $message;
			}
		?>    
		
		
			<form id="delForm" name="cmail" action="newsletter_wypisz#wypisz" method="post">
			<input type="hidden" name="cancel" value="1" />
			<fieldset class="newslet">
			    <legend><?php echo __('newsletter form sign out'); ?></legend>

			    <div class="txt_com"><span class="asterisk">*</span><?php echo __('required fields')?></div>
			    <div class="formL">
				<label for="delFirstName" class="formLabel"><span class="asterisk">*</span><?php echo __('firstname'); ?>: </label>
			    </div>
			    <div class="formR">
				<input id="delFirstName" type="text" class="inText" name="imie" size="35" maxlength="50" value="" /><span id="delFirstNameMsg" class="msgMarg"></span>
			    </div>
			    <br class="clear" />
			    
			    <div class="formL">
				<label for="delEmail" class="formLabel"><span class="asterisk">*</span><?php echo __('email'); ?>: </label>
			    </div>
			    <div class="formR">
				<input id="delEmail" type="text" class="inText" name="email" size="35" maxlength="50" value="" /><span id="delEmailMsg" class="msgMarg"></span>
			    </div>
			    <br class="clear" />
			    
			    <div class="formL"></div>
			    <div class="formR">
				<input type="submit" value="<?php echo __('signout action'); ?>" name="send" class="butForm" />
			    </div>
			    <br class="clear" />
		
			</fieldset>
			</form> 
		<?php
	}
?>
<script type="text/javascript">
    $(document).ready(function() {
	var form = $('#addForm');
	form.submit(function() {
	    if (validateAddFirstName() && validateAddSecondName() && validateAddEmail()){
		//return true;
	    } else {
	       return false;
	    }
	});
	
	$('#addFirstName').blur(validateAddFirstName);
	function validateAddFirstName(){
	    var value = $('#addFirstName').val();
	    if (value == ''){
		$('#addFirstName').addClass('inError');
		$('#addFirstNameMsg').addClass('msgError').text('<?php echo __('error firstname'); ?>');
		return false;
	    } else {
		$('#addFirstName').removeClass('inError');
		$('#addFirstNameMsg').removeClass('msgError').text('');
		return true;
	    }
	}
	
	$('#addSecondName').blur(validateAddSecondName);
	function validateAddSecondName(){
	    var value = $('#addSecondName').val();
	    if (value == ''){
		$('#addSecondName').addClass('inError');
		$('#addSecondNameMsg').addClass('msgError').text('<?php echo __('error lastname'); ?>');
		return false;
	    } else {
		$('#addSecondName').removeClass('inError');
		$('#addSecondNameMsg').removeClass('msgError').text('');
		return true;
	    }
	}

	$('#addEmail').blur(validateAddEmail);
	function validateAddEmail(){
	    var exp = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
	    var email = $("#addEmail").val();
	    if (!exp.test(email)){
		$('#addEmail').addClass('inError');
		$('#addEmailMsg').addClass('msgError').text('<?php echo __('error incorrect email'); ?>');
		return false;
	    } else {
		$('#addEmail').removeClass('inError');
		$('#addEmailMsg').removeClass('msgError').text('');
		return true;
	    }
	}
	
	var form = $('#delForm');
	form.submit(function(){
	    if (validateDelFirstName() && validateDelEmail()){
		//return true;
	    } else {
	       return false;
	    }
	});

	$('#delFirstName').blur(validateDelFirstName);
	function validateDelFirstName(){
	    var value = $('#delFirstName').val();
	    if (value == ''){
		$('#delFirstName').addClass('inError');
		$('#delFirstNameMsg').addClass('msgError').text('<?php echo __('error firstname'); ?>');
		return false;
	    } else {
		$('#delFirstName').removeClass('inError');
		$('#delFirstNameMsg').removeClass('msgError').text('');
		return true;
	    }
	}
	
	$('#delEmail').blur(validateDelEmail);
	function validateDelEmail(){
	    var exp = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
	    var email = $("#delEmail").val();
	    if (!exp.test(email)){
		$('#delEmail').addClass('inError');
		$('#delEmailMsg').addClass('msgError').text('<?php echo __('error incorrect email'); ?>');
		return false;
	    } else {
		$('#delEmail').removeClass('inError');
		$('#delEmailMsg').removeClass('msgError').text('');
		return true;
	    }
	}	
	
    });	
</script>
</div>