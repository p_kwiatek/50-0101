<div class="sidebar">
    <div id="modules-top2-mobile"></div>
    
    <div id="sidebar-menu-desktop">
        <div id="sidebar-menu" class="sidebar-menu">
            <h2 class="module-name">
                <svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" width="262" height="55" viewBox="0 0 262 55"><path d="M530,624l247,6,9,42-262,7Z" transform="translate(-524 -624)" fill="#000000" /></svg>
                <span><?php echo __('Menu') ?></span>
            </h2>
            <a id="add-menu" class="anchor" tabindex="-1"></a>
            <?php get_menu_tree('mg', 0, 0, '', false, '', false, true, true); ?>
            <?php
            /*
             * Dynamic menus
             */
            foreach ($menuType as $dynMenu) {
                if ($dynMenu['menutype'] != 'mg' && $dynMenu['menutype'] != 'tm') {
                    if ($dynMenu['active'] == 1) {

                        $menuClass = '';
                        if (strlen($dynMenu['name']) > 30) {
                            $menuClass = ' module-name-3';
                        } else if (strlen($dynMenu['name']) > 17 && strlen($dynMenu['name']) <= 30) {
                            $menuClass = ' module-name-2';
                        } else {
                            $menuClass = ' module-name-1';
                        }
                        ?>

                        <h2 class="module-name <?php echo $menuClass ?>">
                            <svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" width="262" height="55" viewBox="0 0 262 55"><path d="M530,624l247,6,9,42-262,7Z" transform="translate(-524 -624)" fill="#000000" /></svg>
                            <span><?php echo $dynMenu['name'] ?></span>
                        </h2>

                        <?php get_menu_tree($dynMenu['menutype'], 0, 0, '', false, '', false, true, true); ?>
                        <?php
                    }
                }
            }
            ?>
        </div>
    </div>
    <div class="sidebar-modules row">
        <figure id="ornate-image-1" class="ornate-image"></figure>
    <?php
    foreach ($outRowLeftModules as $module) {
        
        $modules_with_icons = array(
            'mod_calendar',
            'mod_stats',
            'mod_kzk',
            'mod_location',
            'mod_contact',
            'mod_forum',
            'mod_timetable',
            'mod_menu',
            'mod_newsletter',
            'mod_gallery',
            'mod_video',
            'mod_jokes',
        );
        
        $modules_color2 = array(
            'mod_forum',
        );
        
        $href = '/^<a.*?href=(["\'])(.*?)\1.*$/';

        $link = '';

        $tmp = get_module($module['mod_name']);

        preg_match($href, $tmp, $m);

        if ($m[2] != '') {
            $link = $m[2];
        } else {
            $link = trans_url_name($module['name']);
        }
        
	?>
        <div class="col-sm-6 col-md-12">
            <div class="module <?php echo in_array($module['mod_name'], $modules_with_icons) ? 'module-with-icon module-common' : 'module-common'; ?><?php echo in_array($module['mod_name'], $modules_color2) ? ' color2' : ''; ?>" id="<?php echo $module['mod_name']; ?>">
                <div class="module-border-top"></div>
                <div class="module-inner">
                    <div class="module-body clearfix">
                        <?php if (in_array($module['mod_name'], $modules_with_icons)): ?>
                            <div class="icon"></div>
                            <h2 class="module-name">
                                <svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" width="262" height="55" viewBox="0 0 262 55"><path d="M530,624l247,6,9,42-262,7Z" transform="translate(-524 -624)" fill="#000000" /></svg>
                                <span><?php echo $module['name']?></span>
                            </h2>
                            <div class="module-content"><?php echo get_module($module['mod_name'])?></div>
                        <?php else: ?>
                            <h2 class="module-name">
                                <svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" width="262" height="55" viewBox="0 0 262 55"><path d="M530,624l247,6,9,42-262,7Z" transform="translate(-524 -624)" fill="#000000" /></svg>
                                <span><?php echo $module['name']?></span>
                            </h2>
                            <div class="module-content"><?php echo get_module($module['mod_name'])?></div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="module-border-bottom"></div>
            </div>
        </div>
    <?php
        }
    ?>
    </div>
    
    <div id="modules-bottom-mobile"></div>
    
    <?php
    if (count($leftAdv) > 0)
    {
	?>
	<div id="advertsLeftWrapper">
	    <div id="advertsLeftContent">
	<?php
	$n = 0;
	foreach ($leftAdv as $adv)
	{
	    ?>
	    <div class="advertLeft">
	    <?php
	    $extUrl = '';
	    if ($adv['ext_url'] != '')
	    {
		$newWindow = '';
		if ($adv['new_window'] == 1)
		{
		    $newWindow = ' target="_blank" ';
		}
	    			
		$swfLink = '';
		$swfSize = '';
		if ($adv['attrib'] != '' && $adv['type'] == 'flash')
		{
		    //$swfLink = ' style="width:'.$swfSize[0].'px; height:'.$swfSize[1].'px; z-index:1; display:block; position:absolute; left:0; top:0" ';
		}
					
		$extUrl = '<a href="'.$adv['ext_url'].'"'.$newWindow.$swfLink.'>';
		$extUrlEnd = '</a>';
	    }
				
	    switch ($adv['type'])
	    {
		case 'flash':
		    $swfSize = explode(',', $adv['attrib']);
		    //echo $extUrl;
		    ?>
		    <div id="advLeft_<?php echo $n?>"><?php echo $adv['attrib']?></div>
		    <script type="text/javascript">
                        swfobject.embedSWF("<?php echo $adv['content'] . '?noc=' . time()?>", "advLeft_<?php echo $n?>", "<?php echo $swfSize[0]?>", "<?php echo $swfSize[1]?>", "9.0.0", "expressInstall.swf", {}, {wmode:"transparent"}, {});
		    </script>
		    <?php			
		    //echo $extUrlEnd;
		    break;
					
		case 'html':
		    echo $adv['content'];
		    break;
					
		case 'image':
		default:
		    $width = '';
		    if ($adv['content'] != '')
		    {                    
                        $size = getimagesize($adv['content']);
                        if ($size[0] > $templateConfig['maxWidthLeftAdv'])
                        {
                            $width = ' width="'.$templateConfig['maxWidthLeftAdv'].'" ';
                        } 
                        echo $extUrl;
                        ?>
                        <img src="<?php echo $adv['content']?>" alt="<?php echo $adv['name']?>" <?php echo $width?>/>
                        <?php
                        echo $extUrlEnd;
		    }
		    break;
	    }
	    ?>
	    </div>
	    <?php
	    $n++;
	}
	?>
	    </div>
	    
	</div>
        
	<?php
    }
    ?>

</div>
