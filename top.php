<a id="top"></a>    
<ul class="skip-links list-unstyled">
    <li><a href="#main-menu"><?php echo __('skip to main menu')?></a></li> 
    <li><a href="#add-menu"><?php echo __('skip to additional menu')?></a></li> 
    <li><a href="#content"><?php echo __('skip to content')?></a></li>
    <li><a href="#search"><?php echo __('skip to search')?></a></li>
    <li><a href="mapa_strony"><?php echo __('sitemap')?></a></li>
</ul>

<div id="popup"></div>

<div class="top-wrapper">
    <div class="header-wrapper clearfix">
      
        <div class="menu-top" id="menu-hamburger">  
            <p class="sr-only"><?php echo __('main menu'); ?></p>
            <div class="navbar-header">
                <button class="navbar-toggle collapsed" aria-controls="navbar-top" aria-expanded="false" data-target="#navbar-top" data-toggle="collapse" type="button">
                    <i class="icon" aria-hidden="true"></i>
                    <i class="icon" aria-hidden="true"></i>
                    <i class="icon" aria-hidden="true"></i>
                    <span class="sr-only"><?php echo __('expand')?></span>
                    <span class="title sr-only"><?php echo __('main menu')?></span>
                </button>
            </div>
        </div>

        <?php
        include_once ( CMS_TEMPL . DS . 'toolbar.php');
        ?>

        <div id="header" class="header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="banner">     
                            <div id="banner">
                                <div id="headerBanner">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="hero__vector--outer" preserveAspectRatio="none" viewBox="0 0 1155 411"><path d="M231,452L293,23,1386,48l-63,424Z" transform="translate(-231 -23)" fill="#ffffff" /></svg>
                                    <?php if ($outBannerTopRows == 0): ?>
                                        <?php
                                            $img = $templateDir . '/' . 'images' . '/' . 'banner-default.jpg';
                                        ?>
                                        <svg class="hero__vector--inner" viewBox="0 0 1012 411" xmlns="http://www.w3.org/2000/svg" version="1.1">
                                            <g>
                                                <clipPath id="hero-img-def">
                                                    <path d="M426,452L334,48H1346l-37,411Z" transform="translate(-334 -48)"></path>
                                                </clipPath>
                                            </g>
                                            <image clip-path="url(#hero-img-def)" height="100%" width="100%" preserveAspectRatio="xMaxYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $img ?>"></image>
                                        </svg>
                                    <?php elseif ($outBannerTopRows == 1): ?>
                                        <?php
                                            $value = $outBannerTop[0];
                                            $img = 'files' . '/' . $lang . '/' . 'mini' . '/' . $value['photo'];
                                        ?>
                                        <svg class="hero__vector--inner" viewBox="0 0 1012 411" xmlns="http://www.w3.org/2000/svg" version="1.1">
                                            <g>
                                                <clipPath id="hero-img-single">
                                                    <path d="M426,452L334,48H1346l-37,411Z" transform="translate(-334 -48)"></path>
                                                </clipPath>
                                            </g>
                                            <image clip-path="url(#hero-img-single)" height="100%" width="100%" preserveAspectRatio="xMaxYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $img ?>"></image>
                                        </svg>
                                        <?php else: ?>
                                        <div class="carousel-content owl-carousel">
                                            <?php 
                                                $heroSlide = 0;
                                            ?>
                                            <?php foreach ($outBannerTop as $value): ?>
                                                <?php
                                                    $heroSlide++;
                                                    $img = 'files' . '/' . $lang . '/' . 'mini' . '/' . $value['photo'];
                                                ?>
                                                <svg class="hero__vector--inner" viewBox="0 0 1012 411" xmlns="http://www.w3.org/2000/svg" version="1.1">
                                                    <g>
                                                        <clipPath id="<?php echo 'hero-img' . $heroSlide ?>">
                                                            <path d="M426,452L334,48H1346l-37,411Z" transform="translate(-334 -48)"></path>
                                                        </clipPath>
                                                    </g>
                                                    <image clip-path="<?php echo 'url(#hero-img' . $heroSlide . ')'; ?>" height="100%" width="100%" preserveAspectRatio="xMaxYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $img ?>"></image>
                                                </svg>
                                            <?php endforeach ?>
                                        </div>
                                    <?php endif ?>
                                </div>
                                <div class="header__info">
                                    <div>
                                        <?php
                                        $pageInfo['name'] = str_replace('w ', 'w&nbsp;', $pageInfo['name']);
                                        $pageInfo['name'] = str_replace('im ', 'im&nbsp;', $pageInfo['name']);
                                        ?>
                                        <h1 class="headline"><?php echo $pageInfo['name']; ?></h1>
                                        <p class="title"><?php echo __('address'); ?></p>
                                        <?php
                                        echo $headerAddress;
                                        if ($pageInfo['email'] != '') {
                                            echo '<a href="mailto:' . $pageInfo['email'] . '">' . $pageInfo['email'] . '</a>';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        <div id="menu-top" class="menu-top">
            <a id="main-menu" tabindex="-1"></a>
            <nav class="navbar">
                <div id="navbar-top" class="collapse navbar-collapse menu clearfix">
                    <?php get_menu_tree('tm', 0, 0, '', false, '', false, false, false, true, true); ?>
                </div>
            </nav>
        </div>

    </div>

    <div class="page-content page-content-mobile">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div id="content-mobile"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div id="sidebar-menu-mobile" class="sidebar"></div>
                </div>
            </div>
        </div>
    </div>

    <?php include_once('modules_top.php'); ?>

</div>
