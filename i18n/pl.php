<?php
$i18n_texts = array(
    'skip to main menu' => 'Przejdź do menu głównego',
    'skip to additional menu' => 'Przejdź do menu dodatkowego',
    'skip to content' => 'Przejdź do treści',
    'skip to search' => 'Przejdź do wyszukiwarki',
    'sitemap' => 'Mapa strony',
    
    'expand' => 'Rozwiń',
    'collapse' => 'Zwiń',
    
    'main menu' => 'Menu główne',
    'bip' => 'Biuletyn Informacji Publicznej',
    
    'font default' => 'Domyślna czcionka',
    'font bigger' => 'Większa czcionka',
    'font biggest' => 'Największa czcionka',
    'contrast version' => 'Wersja kontrastowa',
    'graphic version' => 'Wersja graficzna',

    'contact' => 'Kontakt',
    
    'search' => 'Wyszukiwarka',
    'open search' => 'Otwórz wyszukiwarkę',
    'search query' => 'Wyszukiwana fraza',
    'search action' => 'Szukaj',
    
    'menu' => 'Menu',
    'additional menu' => 'Menu dodatkowe',
    
    'you are here' => 'Jesteś tutaj',
    
    'author' => 'Autor',
    
    'board info' => 'Dzisiaj na tablicy',
    'news' => 'Aktualności',
    
    'first page' => 'Pierwsza strona',
    'prev page' => 'Poprzednia strona',
    'next page' => 'Następna strona',
    'last page' => 'Ostatnia strona',
    'page' => 'Strona',
    
    'designed' => 'Opracowanie',
    'go to top' => 'Przejdź do góry',
    
    'page requires login' => 'Strona wymaga zalogowania',
    'opens in new window' => 'Otwiera się w nowym oknie',
    
    'image' => 'Obraz',
    'enlarge image' => 'Powiększ obraz',
    'close gallery' => 'Zamknij powiększenie',
    'prev gallery' => 'Poprzedni obraz',
    'next gallery' => 'Następny obraz',
    
    'read' => 'Czytaj',
    'more' => 'więcej',
    'about' => 'o',
    
    'files' => 'Pliki do pobrania',
    'gallery' => 'Galeria zdjęć',
    
    'required' => 'wymagane',
    'required fields' => 'pola wymagane',
    
    'login action' => 'Zaloguj się',
    'login' => 'Login',
    'password' => 'Hasło',
    'repeat password' => 'Powtórz hasło',
    'password forgot' => 'Nie pamiętam hasła',
    'register action' => 'Zarejestruj się',
    'update action' => 'Aktualizuj',
    'gender' => 'Płeć',
    'man' => 'Mężczyzna',
    'woman' => 'Kobieta',
    'avatar' => 'Avatar',
    'delete avatar' => 'Usuń avatar',
    'choose' => 'Wybierz',
    'file info' => 'Pliki .jpg, .gif, .png. Max. rozmiar 500 KB.',
    'edit action' => 'Edytuj konto',
    'password change info' => 'Pozostaw puste jeśli nie chcesz zmieniać.',
    
    'error min length login' => 'Minimalna długość loginu to 4 znaki',
    'error min length password' => 'Minimalna długość hasła to 8 znaków',
    'error incorrect email' => 'Nieprawidłowy adres e-mail',
    'error passwords dont match' => 'Hasła nie zgadzają się',
    
    'new password' => 'Nowe hasło',
    'generate new password info' => 'Aby wygenerować nowe hasło, wpisz w poniższe pole adres e-mail podany podczas rejestracji.',
    'email' => 'E-mail',
    
    'send' => 'Wyślij',
    
    'forum home page' => 'Strona główna forum',
    'home page' => 'Strona główna',
    
    'for' => 'dla',
    'no lessons' => 'Brak zajęć',
    'no' => 'Lp.',
    'hour' => 'Godzina',
    'lesson' => 'Lekcja',
    'teacher' => 'Nauczyciel',
    'room' => 'Sala',
    'show bigger map' => 'Wyświetl większą mapę',
    
    'votes sum' => 'Liczba oddanych głosów',
    
    'no jokes info' => 'Nikt nie dodał jeszcze żadnego żartu. Bądź pierwszy!',
    'add joke' => 'Dodaj żart',
    'nick' => 'Nick',
    'content' => 'Treść',
    'add' => 'Dodaj',
    'added by' => 'Dodał/a',
    'day added' => 'dnia',
    
    'math info' => 'W celu uniemożliwienia rozsyłania spam\'u prosimy rozwiązać proste zadanie matematyczne, np.: 2 + 3 daje 5.',
    'math result' => 'Wynik działania',
    'is' => 'daje',
    
    'no photo album added' => 'Nie dodano jeszcze żadnego albumu.',
    
    'firstname' => 'Imię',
    'lastname' => 'Nazwisko',
    
    'newsletter sign in' => 'Zapisz się na listę mailigową',
    'newsletter form sign in' => 'Formularz zapisania się na listę mailingową',
    'signin action' => 'Zapisz się',
    'newsletter sign out' => 'Wypisz się z listy mailingowej',
    'newsletter form sign out' => 'Formularz wypisania się z listy mailingowej',
    'signout action' => 'Wypisz się',
    
    'error firstname' => 'Wpisz swoje imię',
    'error lastname' => 'Wpisz swoje nazwisko',
    
    'articles' => 'Artykuły',
    
    'title' => 'Tytuł',
    'topics' => 'Wątki',
    'posts' => 'Odpowiedzi',
    'no topics' => 'Brak wątków',
    'add new main topic' => 'Dodaj nowy wątek główny',
    'add new current topic' => 'Dodaj nowy wątek w bieżącym wątku',
    'add new topic' => 'Dodaj nowy wątek',
    'add action' => 'Dodaj',
    'add new post' => 'Dodaj nową odpowiedź',
    'respond action' => 'Odpowiedz',
    'cite and respond action' => 'Zacytuj i odpowiedz',
    'show responds' => 'Pokaż odpowiedzi',
    'no posts' => 'Brak odpowiedzi',
    
    'error topic author' => 'Wpisz autora wątku',
    'error topic title' => 'Wpisz tytuł wątku',
    'error topic content' => 'Wpisz treść wątku',
    'error post author' => 'Wpisz autora odpowiedzi',
    'error post content' => 'Wpisz treść odpowiedzi',
    
    'ie8 info' => 'Strona nie jest obsługiwana przez przeglądarkę Internet Explorer w wersji 8 i niższej',
    
    'font contrast' => 'Kontrast',
    'font size' => 'Czcionka',
    'address' => 'Adres',
    'welcome page' => 'Witamy na stronie placówki',
    'read more' => 'Czytaj więcej',
    'logged out' => 'Wylogowany',
);
