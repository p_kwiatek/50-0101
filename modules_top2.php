<?php if ($numRowTop2Modules > 0): ?>
<div id="modules-top2" class="modules-content modules-top2">
    <?php
        $href = '/^<a.*?href=(["\'])(.*?)\1.*$/';
        
        $links = array();
        
        for ($i = 0; $i < $numRowTop2Modules; $i++) {
            $tmp = get_module($outRowTop2Modules[$i]['mod_name']);
            
            preg_match($href, $tmp, $m);

            if ($m[2] != '') {
                $links[] = $m[2];
            } else {
                $links[] = trans_url_name($outRowTop2Modules[$i]['name']);
            }
        }
        
        $modules_color2 = array(
            'mod_forum',
        );
        
        $module_grid_classes = array(1 => "col-sm-12", 2 => "col-sm-6", 3 => "col-sm-4");
        $module_grid_class = array_key_exists($numRowTop2Modules, $module_grid_classes) ? $module_grid_classes[$numRowTop2Modules] : $module_grid_classes[3];
    ?>
    
    <?php for ($i = 0; $i < $numRowTop2Modules; $i++): ?>
        <a href="<?php echo $links[$i]; ?>" class="module module-with-icon module-common <?php echo in_array($outRowTop2Modules[$i]['mod_name'], $modules_color2) ? 'color2' : ''; ?>" id="<?php echo $outRowTop2Modules[$i]['mod_name']; ?>">
            <div class="module-inner">
                <div class="module-body">
                    <svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid meet" viewBox="0 0 262 195">
                        <g>
                            <clipPath id="<?php echo 'modulesTopImg_' . $outRowTop2Modules[$i]['mod_name']; ?>">
                                <path d="M541,1004L524,809l262,11L773,986Z" transform="translate(-524 -809)"></path>
                            </clipPath>
                        </g>
                        <image clip-path="<?php echo 'url(#modulesTopImg_' . $outRowTop2Modules[$i]['mod_name'] . ')'?>" height="100%" width="100%" preserveAspectRatio="xMaxYMid slice" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php echo $templateDir . '/images/modules/' . $outRowTop2Modules[$i]['mod_name'] . '.jpg' ?>"></image>
                    </svg>
                    <h2 class="module-name">
                        <svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" width="262" height="55" viewBox="0 0 262 55"><path d="M530,624l247,6,9,42-262,7Z" transform="translate(-524 -624)" fill="#000000" /></svg>
                        <span><?php echo $outRowTop2Modules[$i]['name'] ?></span>
                    </h2>
                </div>
            </div>
        </a>
    <?php endfor; ?>
</div>
<?php endif; ?>