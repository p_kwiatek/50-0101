/*
 * Calendar reload
 */

$(document).on("click", ".caption_nav_prev a", function ()
{
    var date = $(this).attr('id').substr(2);
    $('#mod_calendar2 .module_calendar').load('index.php?c=get_calendar&date=' + date);

    $('#calendarNews').load('index.php?c=get_calendar&date=' + date);
});

$(document).on("click", ".caption_nav_next a", function ()
{
    var date = $(this).attr('id').substr(2);
    $('#mod_calendar2 .module_calendar').load('index.php?c=get_calendar&date=' + date);

    $('#calendarNews').load('index.php?c=get_calendar&date=' + date);
});

$(document).ready(function () {
    
    var isMobileView = false;
    var isModuleHeightAlign = false;
    
    updateUI();
    // resizeModule();
    moduleHeightAlign();
    
    $('#goTop').on('click', function(e) {
        e.preventDefault();
        $('html,body').animate({ scrollTop: 0 }, 'slow');
        return false; 
    });

    function hasSelectedLink() {
        if ($('.menus').find('.selected').length > 0) {
            $('.selected').parents('li').children('a').addClass('has-selected');
        }

        $('.sidebar a.selected').parents('li').children('ul').show();
    }

    hasSelectedLink();

    function equalizeHeights(selector) {
        var heights = new Array();
        $(selector).each(function() {
            $(this).css('min-height', '0');
            $(this).css('max-height', 'none');
            $(this).css('height', 'auto');
            heights.push($(this).height());
        });

        var max = Math.max.apply( Math, heights );

        $(selector).each(function() {
            $(this).css('height', max + 20 + 'px');
        }); 
    }

    function equalizeHeightsWithPadding(selector) {
        var heights = new Array();
        $(selector).each(function() {
            $(this).css('min-height', '0');
            $(this).css('max-height', 'none');
            $(this).css('height', 'auto');
            heights.push($(this).outerHeight());
        });

        var max = Math.max.apply( Math, heights );

        $(selector).each(function() {
            $(this).css('height', max + 20 + 'px');
        }); 
    }

    $(window).load(function() {
        equalizeHeights('.headmodules__list h2');
        var iv = null;
        equalizeHeightsWithPadding('.main, .aside');
        $(window).resize(function() {

            if(iv !== null) {
                window.clearTimeout(iv);
            }

            iv = setTimeout(function() {
                    equalizeHeights('.headmodules__list h2');
            }, 120);
            if (Modernizr.mq('(min-width: 992px)') && Modernizr.mq('(max-width: 1199px)')) {    
                equalizeHeightsWithPadding('.main, .aside');
            }
            else if (Modernizr.mq('(min-width: 1200px)')) {    
                equalizeHeightsWithPadding('.main, .aside');
            }
            if (Modernizr.mq('(min-width: 992px)')) {
                equalizeHeightsWithPadding('.aside, .main');
            }
        });
    });

     if (($('.headmodules__list > li').length) > 1) {
        equalizeHeights();        
    }

    function countHeadmodules() {
        switch($('.headmodules__list > li').length) {
            case 1:
                $('.headmodules__list > li').addClass('single');
                break;
            case 2:
                $('.headmodules__list > li').addClass('two');
                break;
            case 3:
               $('.headmodules__list > li').addClass('three');
                break;
        }
    }

    function watch() {
        var s = Snap(document.getElementById("clock"));
        var seconds = s.select("#handSecond"),
            minutes = s.select("#handMinute"),
            hours = s.select("#handHour"),
            face = {
                elem: s.select("#face"),
                cx: s.select("#face").getBBox().cx,
                cy: s.select("#face").getBBox().cy
            },
            angle = 0;
        function update() {
            var time = new Date();
            setHours(time);
            setMinutes(time);
            setSeconds(time);
        }
        function setHours(t) {
            var hour = t.getHours();
            hour %= 12;
            hour += Math.floor(t.getMinutes()/10)/6;
            var angle = hour*360/12;
            hours.animate(
                {transform: "rotate("+angle+" "+face.cx+" "+face.cy+")"},
                100,
                mina.linear,
                function(){
                    if (angle === 360) {
                        hours.attr({transform: "rotate("+0+" "+face.cx+" "+face.cy+")"});               
                    }
                }
              );
        }
        function setMinutes(t) {
            var minute = t.getMinutes();
            minute %= 60;
            minute += Math.floor(t.getSeconds()/10)/6;
            var angle = minute*360/60;
            minutes.animate(
                {transform: "rotate("+angle+" "+face.cx+" "+face.cy+")"},
                100,
                mina.linear,
                function() {
                    if (angle === 360) {
                        minutes.attr({transform: "rotate("+0+" "+face.cx+" "+face.cy+")"});
                    }
                }
            );
        }
        function setSeconds(t) {
            t = t.getSeconds();
            t %= 60;
            var angle = t*360/60;
            if (angle === 0) angle = 360;
            seconds.animate(
                {transform: "rotate("+angle+" "+face.cx+" "+face.cy+")"},
                600,
                mina.elastic,
                function(){
                    if (angle === 360) {
                        seconds.attr({transform: "rotate("+0+" "+face.cx+" "+face.cy+")"});     
                    }
                }
            );
        }
        update();
        setInterval(update, 1000);
    }

    if ($('.clock-container').length > 0) {
        watch();
    }

    countHeadmodules();

    function countModules() {
        switch($('#modules-top2 > a').length) {
            case 1:
                $('#modules-top2').addClass('single');
                break;
            case 2:
                $('#modules-top2').addClass('two');
                break;
            case 3:
               $('#modules-top2').addClass('three');
                break;
        }

        switch($('#modules-bottom > a').length) {
            case 1:
                $('#modules-bottom').addClass('single');
                break;
            case 2:
                $('#modules-bottom').addClass('two');
                break;
            case 3:
               $('#modules-bottom').addClass('three');
                break;
        }
    }

    countModules();

    function modulesMain() {
        var single = $('.modules-content.single .module-body > svg');
        var singlePath = $('.modules-content.single .module-body > svg path');
        var two = $('.modules-content.two .module-body > svg');
        var twoPath = $('.modules-content.two .module-body > svg path');
        if (Modernizr.mq('(min-width: 1200px)')) {
            $(single).removeAttr('viewBox');
            $(singlePath).removeAttr('d');
            $(single).each(function () { $(this)[0].setAttribute('viewBox', '0 0 870 215')});
            $(singlePath).each(function () { $(this)[0].setAttribute('d', 'M541,1004L524,809l850,11L1285,1026Z')});
            $(two).removeAttr('viewBox');
            $(twoPath).removeAttr('d');
            $(two).each(function () { $(this)[0].setAttribute('viewBox', '0 0 410 195')});
            $(twoPath).each(function () { $(this)[0].setAttribute('d', 'M541,1004L524,809l410,11L858,1156Z')});
        }
        else if (Modernizr.mq('(min-width: 992px)') && Modernizr.mq('(max-width: 1199px)')) {
            $(single).removeAttr('viewBox');
            $(singlePath).removeAttr('d');
            $(single).each(function () { $(this)[0].setAttribute('viewBox', '0 0 732 215')});
            $(singlePath).each(function () { $(this)[0].setAttribute('d', 'M541,1004L524,809l712,11L1155,1026Z')});
            $(two).removeAttr('viewBox');
            $(twoPath).removeAttr('d');
            $(two).each(function () { $(this)[0].setAttribute('viewBox', '0 0 330 195')});
            $(twoPath).each(function () { $(this)[0].setAttribute('d', 'M541,1004L524,809l330,11L838,986Z')});
        }
        else if (Modernizr.mq('(min-width: 768px)') && Modernizr.mq('(max-width: 991px)')) {
            $(single).removeAttr('viewBox');
            $(singlePath).removeAttr('d');
            $(single).each(function () { $(this)[0].setAttribute('viewBox', '0 0 735 215')});
            $(singlePath).each(function () { $(this)[0].setAttribute('d', 'M541,1004L524,809l735,11L1200,1026Z')});
            $(two).removeAttr('viewBox');
            $(twoPath).removeAttr('d');
            $(two).each(function () { $(this)[0].setAttribute('viewBox', '0 0 350 195')});
            $(twoPath).each(function () { $(this)[0].setAttribute('d', 'M541,1004L524,809l350,11L858,986Z')});
        }
        else if (Modernizr.mq('(max-width: 767px)')) {
            $(single).removeAttr('viewBox');
            $(singlePath).removeAttr('d');
            $(single).each(function () { $(this)[0].setAttribute('viewBox', '0 0 262 195')});
            $(singlePath).each(function () { $(this)[0].setAttribute('d', 'M541,1004L524,809l262,11L773,986Z')});
            $(two).removeAttr('viewBox');
            $(twoPath).removeAttr('d');
            $(two).each(function () { $(this)[0].setAttribute('viewBox', '0 0 262 195') });
            $(twoPath).each(function () { $(this)[0].setAttribute('d', 'M541,1004L524,809l262,11L773,986Z')});
        }
    }

    modulesMain();

    if (isMobile.any) {
        $('body').addClass('is-mobile');

        $('.dropdown-submenu', '#navbar-top').on('hide.bs.dropdown', function () {
            return false;
        });
    }

    $(".board table, .main-text table").each(function() {
        var $table = $(this);
        
        if ($table.parent('.table-responsive').length === 0) {
            $table.wrap('<div class="table-responsive"></div>');
        }
    });

    $(".owl-carousel").owlCarousel({
        singleItem: true,
        autoPlay: 1e3 * settings.duration,
        slideSpeed: 1e3 * settings.animationDuration,
        paginationSpeed: 1e3 * settings.animationDuration,
        transitionStyle: settings.transition,
        pagination: false,
        mouseDrag: false,
        touchDrag: false
    });

    function modulesContentLinks() {
        $('.modules-content .module').not('#mod_contact').not('#mod_forum').each(function() {
            if ($(this).find('a').length > 0) {
                var href = $(this).find('a').attr('href');
                $(this).css({'cursor':'pointer'});
                $(this).on('click', function() {
                    window.location.href = href;
                });
            }
        });

        if (('.modules-content #mod_forum .button').length > 0) {
            var href = $('.modules-content #mod_forum .button').attr('href');
            $('.modules-content #mod_forum').css({'cursor':'pointer'});
            $('.modules-content #mod_forum').on('click', function() {
                window.location.href = href;
            });
        }

        $('.modules-content #mod_contact').css({'cursor':'pointer'}).on('click', function() {
            window.location.href = 'kontakt';
        });
    }

    modulesContentLinks();

    function fancyboxInit() {
        var a;
        $("a.fancybox").fancybox({
            overlayOpacity: .9,
            overlayColor: settings.overlayColor,
            titlePosition: "outside",
            titleFromAlt: !0,
            titleFormat: function (a, b, c, d) {
                return '<span id="fancybox-title-over">' + texts.image + " " + (c + 1) + " / " + b.length + "</span>" + (a.length ? " &nbsp; " + a.replace(texts.enlargeImage + ": ", "") : "")
            },
            onStart: function (b, c, d) {
                a = b[c];
            },
            onComplete: function () {
                
            },
            onClosed: function () {
            }
        });
    }

    fancyboxInit();

    if (popup.show)
    {
        $.fancybox(
                popup.content,
                {
                    overlayOpacity: .9,
                    overlayColor: settings.overlayColor,
                    padding: 20,
                    autoDimensions: !1,
                    width: popup.width,
                    height: popup.height,
                    transitionIn: "fade",
                    transitionOut: "fade",
                    onStart: function (a) {
                        $("#fancybox-outer").css({
                            background: popup.popupBackground
                        });
                        $("#fancybox-content").addClass("main-text no-margin");
                    }
                }
        );
    }

    $('#gotoTop').children('a').click(function (e) {
        e.preventDefault();
        $('html, body').animate({scrollTop: 0}, 1000, 'easeInOutCubic');
    });

    $('#searchForm').on('click', 'button', function (e) {
        if (Modernizr.mq('(max-width: 991px)')) {
            e.stopPropagation();

            if (!$(e.delegateTarget).hasClass('show')) {
                e.preventDefault();
                $(e.delegateTarget).addClass('show');
                $(e.delegateTarget).find('input[type=text]').focus();
                $('body').one('click', function (e) {
                    $('#searchForm').removeClass('show');
                });
            }
        }
    });

    var modForumTxt = $('.sidebar-modules #mod_forum .button').text();
    $('<span>' + modForumTxt + '</span>').appendTo('.sidebar-modules #mod_forum .button');
    $('<svg viewBox="0 0 186 40" preserveAspectRatio="none"><use xlink:href="#base-button"></use></svg>').appendTo('.sidebar-modules #mod_forum .button');
    $('<svg viewBox="0 0 186 40" preserveAspectRatio="none"><use xlink:href="#base-button"></use></svg>').appendTo('.sidebar-modules #mod_questionnaire .buttonWrapper');
    $('input[type="submit"]').each(function() {
        $(this).wrap('<div class="input-wrapper"></div>');
        $('<svg viewBox="0 0 186 40" preserveAspectRatio="none"><use xlink:href="#base-button"></use></svg>').appendTo('.input-wrapper');
    });
    $('.link-object').each(function() {
        $('<svg viewBox="0 0 186 40" preserveAspectRatio="none"><use xlink:href="#base-button"></use></svg>').appendTo(this);
    });

    $('a[href=wyloguj_sie]').wrap('<div class="link-wrapper"></div>');
    $('<svg viewBox="0 0 186 40" preserveAspectRatio="none"><use xlink:href="#base-button"></use></svg>').appendTo('.link-wrapper');

    $('#searchForm').on('click', 'input', function (e) {
        e.stopPropagation();
    });
      
    var current_breakpoint = getCurrentBreakpoint();

    $(window).on('resize', function(e) {

        modulesMain();
        
        var _cb = getCurrentBreakpoint();
        
        if (current_breakpoint !== _cb) {
            current_breakpoint = _cb;
            updateUI();
            resizeModule();
            moduleHeightAlign();
            modulesMain();
        }
        
    });


    function updateUI() {
        if (Modernizr.mq('(max-width: 991px)') && !isMobileView) {
            isMobileView = true;
            $('.header-address', '#banner').prependTo('.header-name', '#banner');
            $('.header-image-1').prependTo('.header-address', '#banner');
            $('.header-image-2').prependTo('.header-welcome .message');
            $('#searchbar').insertAfter('#fonts');
            // $('#menu-top').insertBefore('#toolbar');
            $('#content').prependTo('#content-mobile');
            $('#sidebar-menu').prependTo('#sidebar-menu-mobile');
            $('#modules-top2').prependTo('#modules-top2-mobile');
            $('#modules-bottom').prependTo('#modules-bottom-mobile');
        } else if (Modernizr.mq('(min-width: 992px)') && isMobileView) {
            isMobileView = false;
            $('.header-address', '#banner').insertAfter('.header-name', '#banner');
            $('.header-image-1').insertAfter('.header-address', '#banner');
            $('.header-image-2').insertAfter('.header-address', '#banner');
            $('#searchbar').prependTo('#searchbar-desktop');
            // $('#menu-top').insertAfter('#toolbar');
            $('#content').prependTo('#content-desktop');
            $('#sidebar-menu').prependTo('#sidebar-menu-desktop');
            $('#modules-top2').prependTo('#modules-top2-desktop');
            $('#modules-bottom').prependTo('#modules-bottom-desktop');
        }
    }

    function moduleHeightAlign() {
        if (Modernizr.mq('(min-width: 768px)') && Modernizr.mq('(max-width: 991px)')) {

            var maxHeight = 0;
            var maxHeightModuleId = null;

            $('.sidebar-modules').find('.module').each(function () {
                var $this = $(this);
                if ($this.find('.module-body').height() > maxHeight) {
                    maxHeight = $this.find('.module-body').outerHeight();
                    maxHeightModuleId = $this.attr('id');
                }
            });

            $('.sidebar-modules').find('.module').each(function () {
                var $this = $(this);

                var moduleId = $this.attr('id');

                if (maxHeightModuleId !== moduleId) {

                    $this.find('.module-body').outerHeight(maxHeight);

                }
            });

            isModuleHeightAlign = true;

        } else if (isModuleHeightAlign) {

            $('.sidebar-modules').find('.module').each(function () {
                var $this = $(this);
                $this.outerHeight('auto');

            });

            isModuleHeightAlign = false;
        }
    }

    $('#navbar-top').on('click', '.dropdown-submenu > a', function (e) {

        var $this = $(this);
        var $submenu = $this.next('.dropdown-menu');

        if ($submenu.length > 0) {
            if (isMobile.any) {

                if ($this.parent().hasClass('open')) {
                    window.location.href = $this.attr('href');
                }

                $this.parent().addClass('open');

            } else {
                if ($this.parent().hasClass('open')) {
                    window.location.href = $this.attr('href');
                }
            }
        }

    });

    $('.menus', '#sidebar-menu').on('click', 'a', function (e) {

        var $this = $(this);
        var $sublist = $this.next('.dropdown-menu');

        if ($sublist.is(':hidden')) {
            e.preventDefault();
            $sublist.show();
        }

    });
    
});

$(function() {
    var a = new Date,
        b = new Date;
    $(document).on("focusin", function(c) {
        $(".keyboard-outline").removeClass("keyboard-outline");
        var d = b < a;
        d && $(c.target).addClass("keyboard-outline");
    }), $(document).on("click", function() {
        b = new Date;
    }), $(document).on("keydown", function() {
        a = new Date;
    });
});

function resizeModule() {
        $('.module.module-common').each(function() {
            var $this = $(this);
            var $parent = $this.parent();
            
            $this.find('.module-body').outerHeight('auto');

            // var width = Math.round($parent.width());
            // var widthOverflow = (width-30) % 20;

            // if (widthOverflow) {
            //     $this.width(width-widthOverflow);
            // }
            
            var height = Math.round($this.find('.module-body').outerHeight());
            var heightOverflow = height % 20;
            var heightComplement = 20 - heightOverflow;

            if (heightOverflow) {
                $this.find('.module-body').height(height+heightComplement);
            }
        });
}

function getCurrentBreakpoint() {
    
    var breakpoints = [0, 768, 992, 1200].reverse();
    
    for (var i=0; i < breakpoints.length; i++) {
        if (Modernizr.mq('(min-width: ' + breakpoints[i] + 'px)')) {
            return i;
        }
    }
}
